﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _2048
{
    class Program
    {

        public static Random Rand = new Random();

        public static int[,] Grille = new int[4, 4];

        public static Dictionary<int, ConsoleColor> Colors = new Dictionary<int, ConsoleColor>
        { 
            {2, ConsoleColor.Cyan },
            {4, ConsoleColor.Blue },
            {8, ConsoleColor.Green },
            {16, ConsoleColor.Red },
            {32, ConsoleColor.DarkCyan },
            {64, ConsoleColor.DarkBlue },
            {128, ConsoleColor.DarkGreen },
            {256, ConsoleColor.DarkRed },
            {512, ConsoleColor.Magenta },
            {1024, ConsoleColor.DarkMagenta },
            {2048, ConsoleColor.DarkYellow },
            {4096, ConsoleColor.DarkYellow },
            {8192, ConsoleColor.DarkYellow },
        };

        static void Main(string[] args)
        {
            while(CanPlay())
            {
                DisplayEmptyGrid();
                GetRandomTile();
                DisplayTiles();
                ConsoleKey key = Console.ReadKey(true).Key;
                Grille = MergeCells(key);
            }
        }

        public static int[,] MergeCells(ConsoleKey key)
        {
            List<int>[] lines = new List<int>[4]; 
            switch (key)
            {
                case ConsoleKey.UpArrow:
                    for(int j= 0; j < 4; j++) {
                        lines[j] = new List<int>();
                        for (int i = 0; i < 4 ; i++)
                        {
                            if (Grille[j, i] != 0)
                                lines[j].Add(Grille[j, i]);
                        }
                    }
                    break;
                case ConsoleKey.DownArrow:
                    for (int j = 0; j < 4; j++)
                    {
                        lines[j] = new List<int>();
                        for (int i = 3; i >= 0; i--)
                        {
                            if (Grille[j, i] != 0)
                                lines[j].Add(Grille[j, i]);
                        }
                    }
                    break;
                case ConsoleKey.LeftArrow:
                    for (int i = 0; i < 4; i++)
                    {
                        lines[i] = new List<int>();
                        for (int j = 0; j < 4; j++)
                        {
                            if (Grille[j, i] != 0)
                                lines[i].Add(Grille[j, i]);
                        }
                    }
                    break;
                case ConsoleKey.RightArrow:
                    for (int i = 0; i < 4; i++)
                    {
                        lines[i] = new List<int>();
                        for (int j = 3; j >= 0; j--)
                        {
                            if (Grille[j, i] != 0)
                                lines[i].Add(Grille[j, i]);
                        }
                    }
                    break;
                 
            }
            List<int>[] newLines = GetNewLines(lines);
            int[,] newGrille = new int[4, 4];

            switch (key)
            {
                case ConsoleKey.UpArrow:
                    for (int j = 0; j < 4; j++)
                    {
                        int c = 0;
                        for (int i = 0; i < newLines[j].Count ; i++)
                        {
                            newGrille[j, i] = newLines[j][c];
                            c++;
                        }
                    }
                    break;
                case ConsoleKey.DownArrow:
                    for (int j = 0; j < 4; j++)
                    {
                        int c = 0;
                        for (int i = 3; i >= 4 - newLines[j].Count; i--)
                        {
                            newGrille[j, i] = newLines[j][c];
                            c++;
                        }
                    }
                    break;
                case ConsoleKey.LeftArrow:
                    for (int i = 0; i < 4; i++)
                    {
                        int c = 0;
                        for (int j = 0; j < newLines[i].Count; j++)
                        {
                            newGrille[j, i] = newLines[i][c];
                            c++;
                        }
                    }
                    break;
                case ConsoleKey.RightArrow:
                    for (int i = 0; i < 4; i++)
                    {
                        int c = 0;
                        for (int j = 3; j >= 4 - newLines[i].Count; j--)
                        {
                            newGrille[j, i] = newLines[i][c];
                            c++;
                        }
                    }
                    break;

            }
            return newGrille;
        }

        public static List<int>[] GetNewLines(List<int>[] lines)
        {
            foreach(List<int> l in lines)
            {
                int count = l.Count;
                for (int i = 0; i < count - 1; i++)
                {
                    if(l[i] == l[i+1])
                    {
                        l[i] = l[i] + l[i + 1];
                        l.RemoveAt(i + 1);
                        count--;
                    }
                }
            }
            return lines;
        }

        public static void GetRandomTile()
        {
            int alea = Rand.Next(9);
            int nb = alea < 6 ? 2 : 4;
            int x = Rand.Next(4);
            int y = Rand.Next(4);
            while(Grille[x,y] != 0)
            {
                x = Rand.Next(4);
                y = Rand.Next(4);
            } 
            Grille[x, y] = nb;
        }

        public static bool CanPlay()
        {
            //throw new NotImplementedException();
            return true;
        }

        public static void DisplayTiles()
        {
            for(int i=0; i < Grille.GetLength(0); i++)
            {
                for(int j=0; j < Grille.GetLength(1); j++)
                {
                    if(Grille[j, i] != 0)
                    {
                        Console.SetCursorPosition(j*5+1, i*2 + 1);
                        Console.ForegroundColor = Colors[Grille[j, i]];
                        Console.WriteLine(Grille[j, i]);
                        Console.ForegroundColor = ConsoleColor.Gray;
                    } 
                }
            }
        }

        public static void DisplayEmptyGrid()
        {
            Console.Clear();
            Console.WriteLine("---------------------");
            Console.WriteLine("|    |    |    |    |");
            Console.WriteLine("---------------------");
            Console.WriteLine("|    |    |    |    |");
            Console.WriteLine("---------------------");
            Console.WriteLine("|    |    |    |    |");
            Console.WriteLine("---------------------");
            Console.WriteLine("|    |    |    |    |");
            Console.WriteLine("---------------------");
        }
    }
}
